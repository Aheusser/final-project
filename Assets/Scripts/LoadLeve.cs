﻿using UnityEngine;
using System.Collections;

public class LoadLeve : MonoBehaviour {

    private bool playerInExit;
	
	public string levelToLoad;
    public float waitToReload;
    private bool reloading;
    private GameObject thePlayer;

    // Use this for initialization
    void Start()
	{
		playerInExit = false;
	}

    // Update is called once per frame
    void Update()
    {

        if (reloading)
        {
            waitToReload -= Time.deltaTime;
            if (waitToReload < 0)
            {
                Application.LoadLevel(Application.loadedLevel+1);
                thePlayer.SetActive(true);
            }
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.name == "Player")
        {
            //Destroy (other.gameObject);
            other.gameObject.SetActive(false);
            reloading = true;
            thePlayer = other.gameObject;
        }

    }

}